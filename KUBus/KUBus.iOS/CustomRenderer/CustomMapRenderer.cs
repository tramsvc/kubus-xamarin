﻿using System.ComponentModel;

using CoreLocation;
using MapKit;
using Foundation;
using UIKit;

using KUBus.Controls;
using KUBus.iOS.CustomRenderer;

using Xamarin.Forms;
using Xamarin.Forms.Platform.iOS;
using Xamarin.Forms.Maps.iOS;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace KUBus.iOS.CustomRenderer
{
    public class CustomMapRenderer : MapRenderer
    {
        /// <summary>
        /// Instance of Custom Map control in .NET Shared Code Library
        /// </summary>
        CustomMap customMap;

        /// <summary>
        /// Instance of naitve map of iOS
        /// </summary>
        MKMapView nativeMap;

        /// <summary>
        /// Polyline Renderer on iOS
        /// </summary>
        MKPolylineRenderer polylineRenderer;

        /// <summary>
        /// Override the OnElementChanged() event handler to get desired instance.
        /// </summary>
        /// <param name="e"> New element or Old element.</param>
        protected override void OnElementChanged(ElementChangedEventArgs<View> e)
        {
            base.OnElementChanged(e);

            if (e.OldElement != null)
            {
                nativeMap = Control as MKMapView;
                if (nativeMap != null)
                {
                    nativeMap.RemoveOverlays(nativeMap.Overlays);
                    nativeMap.OverlayRenderer = null;
                    polylineRenderer = null;
                }
            }

            if(e.NewElement != null)
            {
                var formsMap = (CustomMap)e.NewElement;
                nativeMap = Control as MKMapView;
                nativeMap.OverlayRenderer = null;
                UpdatePolyline();
            }
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(Element == null || Control == null)
            {
                return;
            }

            if(e.PropertyName == CustomMap.PolylineCoordinatesProperty.PropertyName
                || e.PropertyName == CustomMap.PolylineColorProperty.PropertyName
                || e.PropertyName == CustomMap.PolylineThicknessProperty.PropertyName)
            {
                UpdatePolyline();
            }
        }

        /// <summary>
        /// Draw Polyline from PolylineCoordinates.
        /// </summary>
        private void UpdatePolyline()
        {
            if(nativeMap != null)
            {
                var formsMap = (CustomMap)Element;
                nativeMap = Control as MKMapView;
                nativeMap.OverlayRenderer = GetOverlayRenderer;

                CLLocationCoordinate2D[] coords = new CLLocationCoordinate2D[formsMap.PolylineCoordinates.Count];

                int index = 0;
                foreach (var position in formsMap.PolylineCoordinates)
                {
                    coords[index] = new CLLocationCoordinate2D(position.Latitude, position.Latitude);
                    index++;
                }

                var routeOverlay = MKPolyline.FromCoordinates(coords);
                nativeMap.AddOverlay(routeOverlay);
            }
        }

        /// <summary>
        /// Return MKOverlay (Line of Polyline) designed basing on the CustomMap instance.
        /// </summary>
        /// <param name="mapView">Instance of MapView.</param>
        /// <param name="overlay">Interface of MKOverlay.</param>
        /// <returns></returns>
        private MKOverlayRenderer GetOverlayRenderer(MKMapView mapView, IMKOverlay overlay)
        {
            if(polylineRenderer == null)
            {
                polylineRenderer = new MKPolylineRenderer(overlay as MKPolyline);
                polylineRenderer.FillColor = customMap.PolylineColor.ToUIColor();
                polylineRenderer.StrokeColor = customMap.PolylineColor.ToUIColor();
                polylineRenderer.LineWidth = (float)customMap.PolylineThickness;
            }
            return polylineRenderer;
        }
    }
}