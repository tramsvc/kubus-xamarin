﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace KUBus.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ResultPage : TabbedPage
    {
        public ResultPage ()
        {
           
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();
            InitializeComponent();
        }
        protected async override void OnDisappearing()
        {
            base.OnDisappearing();
            await Navigation.PopToRootAsync();
        }
    }
}