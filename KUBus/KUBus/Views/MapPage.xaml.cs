﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using KUBus.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace KUBus.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class MapPage : ContentPage
	{
        MapPageViewModel ViewModel;

        public MapPage()
        {
            ViewModel = new MapPageViewModel();
            BindingContext = ViewModel;
            InitializeComponent();
        }

        /*
         * public MapPage (List<string> MockCoordList)
		{
            ViewModel = new MapPageViewModel(MockCoordList);
            BindingContext = ViewModel;
            InitializeComponent();  
        }
        */

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            Navigation.PopToRootAsync();
        }
    }
}