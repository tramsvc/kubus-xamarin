﻿using KUBus.Models;
using KUBus.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace KUBus.Controls
{
    public class CustomMap : Map, INotifyPropertyChanged
    {
        private double lowestLat;
        private double highestLat;
        private double lowestLong;
        private double highestLong;
        private double finalLat;
        private double finalLong;
        private double distance;
        private bool isMapLoaded;

        #region Dictionary
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dictionary"></param>
        public void SaveState(IDictionary<string, object> dictionary)
        {
            dictionary["FinalLat"] = finalLat;
            dictionary["FinalLong"] = finalLong;
            dictionary["Distance"] = distance;
        }
        public void RestoreState(IDictionary<string, object> dictionary)
        {
            finalLat = GetDictionaryEntry(dictionary, "FinalLat", 14.0158603);
            finalLong = GetDictionaryEntry(dictionary, "FinalLong", 99.970538);
            distance = GetDictionaryEntry(dictionary, "Distance",0.7);
        }
        public T GetDictionaryEntry<T>(IDictionary<string, object> dictionary, string key, T defaultValue)
        {
            if (dictionary.ContainsKey(key))
                return (T)dictionary[key];
            return defaultValue;
        }
        #endregion

        /// <summary>
        /// List of steps which will be use as points of interest to place the Camera.
        /// </summary>
        public List<Position> PolylineSteps;

        //Pin CLR Bindable Property Declaration
        public static readonly BindableProperty CustomPinsProperty =
            BindableProperty.Create(nameof(CustomPins), typeof(ObservableCollection<CustomPin>), typeof(CustomMap), null, BindingMode.TwoWay);

        //Pin ObservableCollection
        public ObservableCollection<CustomPin> CustomPins
        {
            get { return (ObservableCollection<CustomPin>)GetValue(CustomPinsProperty); }
            set { SetValue(CustomPinsProperty, value); }
        }

        /// <summary>
        /// This IList<string> is containing all of the address given by the user to create the polyline.
        /// </summary>
        public static readonly BindableProperty PolylineAddressPointsProperty =
            BindableProperty.Create(nameof(PolylineAddressPoints), typeof(IList<string>), typeof(CustomMap), null,
                propertyChanged: OnPolyLineAddressPointsPropertyChanged);

        /// <summary>
        /// Assessor for PolylineAddressPoints property.
        /// </summary>
        public IList<string> PolylineAddressPoints
        {
            get { return (IList<string>)GetValue(PolylineAddressPointsProperty); }
            set { SetValue(PolylineAddressPointsProperty, value); }
        }

        /// <summary>
        /// This List<GeoPosition> is containing all of the Position of the polyline.
        /// </summary>
        public static readonly BindableProperty PolylineCoordinatesProperty =
            BindableProperty.Create(nameof(PolylineCoordinates), typeof(List<GeoPosition>), typeof(CustomMap), null);

        /// <summary>
        /// Assessor for PolylineCoordinates property.
        /// </summary>
        public List<GeoPosition> PolylineCoordinates
        {
            get { return (List<GeoPosition>)GetValue(PolylineCoordinatesProperty); }
            set { SetValue(PolylineCoordinatesProperty, value); }
        }

        /// <summary>
        /// Color of the Polyline.
        /// </summary>
        public static readonly BindableProperty PolylineColorProperty =
            BindableProperty.Create(nameof(PolylineColor), typeof(Color), typeof(CustomMap), Color.Red);

        /// <summary>
        /// Assessor for PolylineColor property.
        /// </summary>
        public Color PolylineColor
        {
            get { return (Color)GetValue(PolylineColorProperty); }
            set { SetValue(PolylineColorProperty, value); }
        }

        /// <summary>
        /// Width of the Polyline.
        /// </summary>
        public static readonly BindableProperty PolylineThicknessProperty =
            BindableProperty.Create(nameof(PolylineThickness), typeof(double), typeof(CustomMap), 5.0);

        /// <summary>
        /// Assessor for PolylineThickness property.
        /// </summary>
        public double PolylineThickness
        {
            get { return (double)GetValue(PolylineThicknessProperty); }
            set { SetValue(PolylineThicknessProperty, value); }
        }

        /// <summary>
        /// Point of interest possibilities.
        /// </summary>
        public enum CameraFocusReference
        {
            None,
            OnPolyline
        }

        /// <summary>
        /// Point of interest for the Camera.
        /// </summary>
        public static readonly BindableProperty CameraFocusParameterProperty =
            BindableProperty.Create(nameof(CameraFocusParameter), typeof(CameraFocusReference), typeof(CustomMap), CameraFocusReference.OnPolyline);

        /// <summary>
        /// Assessor for CameraFocusParameter property.
        /// </summary>
        public CameraFocusReference CameraFocusParameter
        {
            get { return (CameraFocusReference)GetValue(CameraFocusParameterProperty); }
            set { SetValue(CameraFocusParameterProperty, value); }
        }

        /// <summary>
        /// Constructor.
        /// </summary>
        public CustomMap()
        {
            isMapLoaded = false;
            PolylineSteps = new List<Position>();
            PropertyChanged += (object sender, PropertyChangedEventArgs e) =>
            {
                CustomMap map = sender as CustomMap;
            };
            //getCustomMapInstance(this);

        }

        #region OnPolyLineAddressPointsPropertyChanged
        /// <summary>
        /// Method call each time the PolyLineAddressPointsProperty is set.
        /// This method starts the generation of the polyline.
        /// </summary>
        /// <param name="bindable">CustomMap instance.</param>
        /// <param name="oldValue">Previous value of PolyLineAddressPointsProperty.</param>
        /// <param name="newValue">New value of PolyLineAddressPointsProperty.</param>        
        public static void OnPolyLineAddressPointsPropertyChanged(BindableObject bindable, object oldValue, object newValue)
        {
            ((CustomMap)bindable).OnPolyLineAddressPointsPropertyChanged((IList<string>)oldValue, (IList<string>)newValue);
        }
        /// <summary>
        /// Call GeneratePolylineCoordinatesInner() which generates the polyline by the CustomMap itself.
        /// </summary>
        /// <param name="oldValue">Previous value of PolyLineAddressPointsProperty.</param>
        /// <param name="newValue">New value of PolyLineAddressPointsProperty.</param>   
        public void OnPolyLineAddressPointsPropertyChanged(IList<string> oldValue, IList<string> newValue)
        {
            GeneratePolylineCoordinatesInner();
        }
        #endregion

        #region Generation methods
        /// <summary>
        /// Event for updates of map's properties.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Translate the PolylineAddressPoints list to GeoPosition usable to create the polyline. This method, then, fire the changement of the property by PropertyChangedEventArgs.
        /// This method also call CoverFieldOfFocus() which move the camera to the POI of CameraFocusParameterProperty.
        /// </summary>
        public async void GeneratePolylineCoordinatesInner()
        {
            if (PolylineAddressPoints != null)
            {
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(PolylineCoordinatesProperty)));
                PolylineCoordinates = await GeneratePolylineCoordinates(PolylineAddressPoints); 
                CoverFieldOfFocus();
            }
        }
        /// <summary>
        /// This method get every information from the Google API and then, by the help of other method, decode every polylines for each step, get the major step of the polyline.
        /// At the end, this function return a List of GeoPositions corresponding to the polyline of the address mail's list.
        /// This method is also the parser for the data of the Google API.
        /// </summary>
        /// <param name="AddressPoints">Take a List<string> of mail address.</param>
        /// <returns>List of GeoPositions corresponding to the polyline of the address mail's list.</returns>
        public async Task<List<GeoPosition>> GeneratePolylineCoordinates(IList<string> AddressPoints)
        {
            if (AddressPoints.Count > 1)
            {
                List<GeoPosition> GeoPositionList = new List<GeoPosition>();
                List<JObject> DirectionResponseList = new List<JObject>();
                string addr_tmp = null;

                foreach (string item in AddressPoints)
                {
                    if (addr_tmp == null)
                    {
                        addr_tmp = item;
                    }
                    else
                    {
                        JObject response = await Services.GoogleMapServices.GoogleDirectionServices.GetDirectionFromGoogleAPI(addr_tmp, item); //(Origin,Destination)
                        if (response != null)
                        {
                            DirectionResponseList.Add(response);
                        }
                        addr_tmp = item;
                    }
                }
                foreach (JObject item in DirectionResponseList)
                {
                    bool finished = false;
                    int index = 0;

                    try
                    {
                        PolylineSteps.Add(new Position(
                        Double.Parse((item["routes"][0]["legs"][0]["steps"][index]["start_location"]["lat"]).ToString()),
                        Double.Parse((item["routes"][0]["legs"][0]["steps"][index]["start_location"]["lng"]).ToString())));
                    }
                    catch (Exception)
                    {
                        finished = true;
                    }

                    while (!finished)
                    {
                        try
                        {
                            PolylineSteps.Add(new Position(
                            Double.Parse((item["routes"][0]["legs"][0]["steps"][index]["end_location"]["lat"]).ToString()),
                            Double.Parse((item["routes"][0]["legs"][0]["steps"][index]["end_location"]["lng"]).ToString())));

                            GeoPositionList.AddRange(Services.StepDecodeService.DecodeService.Decode((item["routes"][0]["legs"][0]["steps"][index]["polyline"]["points"]).ToString()));
                            index++;
                        }
                        catch (Exception)
                        {
                            finished = true;
                        }
                    }
                }
                int a = 0;
                foreach (GeoPosition gp in GeoPositionList)
                {
                    a++;
                }
                return (GeoPositionList);
            }
            else
            {
                return (new List<GeoPosition>());
            }
        }
        #endregion

        #region Camera focus definition
        /// <summary>
        /// Class data for store information about the focus of the Camera.
        /// </summary>
        public class CameraFocusData
        {
            public Position Position { get; set; }
            public Distance Distance { get; set; }
        }

        /// <summary>
        /// Prepare data for calcul and use DistanceCalculation to make the calcul of the final data.
        /// </summary>
        public void CoverFieldOfFocus()
        {
                if (isMapLoaded)
                {
                    if (CameraFocusParameter == CameraFocusReference.OnPolyline)
                    {
                        List<double> latitudes = new List<double>();
                        List<double> longitudes = new List<double>();
                        if (PolylineSteps.Any())
                        {
                            foreach (Position step in PolylineSteps)
                            {
                                latitudes.Add(step.Latitude);
                                longitudes.Add(step.Longitude);
                            }

                            lowestLat = latitudes.Min();
                            highestLat = latitudes.Max();
                            lowestLong = longitudes.Min();
                            highestLong = longitudes.Max();
                            finalLat = (lowestLat + highestLat) / 2;
                            finalLong = (lowestLong + highestLong) / 2;

                            distance = Services.DistanceCalc.DistanceCalc.CalcDistance(lowestLat, lowestLong, highestLat, highestLong, Services.DistanceCalc.DistanceCalc.GeoCodeCalcMeasurement.Kilometers);
                            MoveToRegion(MapSpan.FromCenterAndRadius(new Position(finalLat, finalLong), Distance.FromKilometers(distance * 0.7)));
                            return;
                        }
                    }
                }
                
        }
        #endregion

        #region Single Town part
        /// <summary>
        /// Instance of the current object for static uses.
        /// </summary>
        private static CustomMap customMapInstance;
        /// <summary>
        /// Get the current object instance or create one if the customMapInstance is null.
        /// </summary>
        /// <param name="instance">Current instance of the object for initialization.</param>
        /// <returns>The instance of the current object.</returns>
        /// 
        /*public static CustomMap getCustomMapInstance(CustomMap instance)
        {
            if (customMapInstance == null)
            {
                customMapInstance = instance;
            }
            return customMapInstance;
        }
        #endregion*/

        public void MapLoaded()
        {
            isMapLoaded = true;
            CoverFieldOfFocus();
        }

        /*
        public Action<CustomPin> OnInfoWindowClicked;

        public List<CustomPin> CustomPins
        {
            get { return (List<CustomPin>)GetValue(CustomPinsProperty); }
            set { SetValue(CustomPinsProperty, value); }
        }

        public List<Position> RouteCoordinates
        {
            get { return (List<Position>)GetValue(RouteCoordinatesProperty); }
            set { SetValue(RouteCoordinatesProperty, value); }
        }

        public Int32 RouteColor
        {
            get { return (Int32)GetValue(RouteColorProperty); }
            set { SetValue(RouteColorProperty, value); }
        }

        public static readonly BindableProperty RouteCoordinatesProperty =
            BindableProperty.Create(nameof(RouteCoordinates), typeof(List<Position>), typeof(CustomMap), new List<Position>(), BindingMode.TwoWay);

        public static readonly BindableProperty RouteColorProperty =
            BindableProperty.Create(nameof(RouteColor), typeof(Int32), typeof(CustomMap), null);

        public static readonly BindableProperty CustomPinsProperty =
            BindableProperty.Create(nameof(CustomPins), typeof(CustomPin), typeof(CustomMap), new List<CustomPin>(), BindingMode.TwoWay);

        public CustomMap()
        {
            RouteCoordinates = new List<Position>();
            //CustomPins = new List<CustomPin>();
        }*/
        #endregion
    }
}