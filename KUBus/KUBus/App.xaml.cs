using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation (XamlCompilationOptions.Compile)]
namespace KUBus
{
	public partial class App : Application
	{
        public static readonly string Google_Map_API_Key = "AIzaSyALEWDnZeeEKb8Jt6Bdk16ZRex9z-dCYV4";

        public App ()
		{
			InitializeComponent();
            MainPage = new NavigationPage(new Views.MainPage());
            /*
            Initialize for Navigation Service
            NavigationService.Configure("MainPage", typeof(Views.MainPage));
            NavigationService.Configure("QRScanPage", typeof(Views.QRScanPage));
            NavigationService.Configure("MapPage", typeof(Views.MapPage));
            var mainPage = ((ViewNavigationService)NavigationService).SetRootPage("MainPage");
            MainPage = mainPage;
            */
        }

        //public static INavigationService NavigationService { get; } = new ViewNavigationService();

        protected override void OnStart ()
		{
			// Handle when your app starts
		}

		protected override void OnSleep ()
		{
			// Handle when your app sleeps
		}

		protected override void OnResume ()
		{
			// Handle when your app resumes
		}
	}
}
