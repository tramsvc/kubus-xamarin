﻿using Xamarin.Forms.Maps;

namespace KUBus.Models
{
    public class CustomPin : Pin
    {
        //Mock up structures.
        public string ID { get; set; }
        public string BusLine { get; set; }
        public string Url { get; set; }
    }
}
