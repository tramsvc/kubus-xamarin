﻿namespace KUBus.Models
{
    public class GeoPosition
    {
        public double Latitude { get; set; }
        public double Longitude { get; set; }

        public GeoPosition()
        {
            Latitude = 0.0;
            Longitude = 0.0;
        }

        public GeoPosition(double latitude, double longitude)
        {
            Latitude = latitude;
            Longitude = longitude;
        }
    }
}
