﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Xamarin.Forms;

using KUBus.Views;

namespace KUBus.ViewModels
{
    public class MainPageViewModel : Base.ViewModelBase
    {
        private string textEntry = string.Empty;
        private string GPSLocatorPosition { get; set; }
        private readonly List<string> TestMockList = new List<string>()
        {
            "14.007736, 99.974291",
            "14.013167, 99.971059"
        };

        //public List<string> MockCoordList { get; set; } = new List<string>();

        public string TextEntry
        {
            get { return textEntry; }
            set
            {
                textEntry = value;
                OnPropertyChanged(nameof(DisplayText));
            }
        }

        public string DisplayText
        {
            get { return $"Hello, {TextEntry}"; }
        }

        //Command Accessors
        public Command NavigateToMapPageCommand { get; }
        public Command NavigateToGPSPageCommand { get; }
        public Command NavigateToQRScanPageCommand { get; }
        public Command NavigateToSecondPageCommand { get; }

        //Constructor
        public MainPageViewModel()
        {
            NavigateToMapPageCommand = new Command(async () => await GoToMapPage());
            NavigateToGPSPageCommand = new Command(async () => await GoToGPSPage());
            NavigateToQRScanPageCommand = new Command(async () => await GoToQRScanPage());
            NavigateToSecondPageCommand = new Command(async () => await GoToSecondScanPage());
        }

        private async Task GoToSecondScanPage()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new MapPage(/*TestMockList*/));
        }

        private async Task GoToQRScanPage()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new QRScanPage());
        }

        private async Task GoToGPSPage()
        {
            IsBusy = true;
            try
            {
                var currentPosition = await Services.GeolocationService.GeolocationService.GetCurrentPostion();
                if (currentPosition == null)
                {
                    PageDialogue.Toast("Cannot get position from GPS. Make sure GPS is enabled on your device.");
                }
                else
                {
                    IsBusy = false;
                    await Application.Current.MainPage.Navigation.PushAsync(new MapPage());
                }
                
            }
            catch(Exception ex)
            {
                PageDialogue.Toast("Error: "+ex.GetType().ToString());
            }
            finally
            {
                IsBusy = false;
            }
        }

        private async Task GoToMapPage()
        {
            await Application.Current.MainPage.Navigation.PushAsync(new MapPage(/*TestMockList*/));
        }
    }
}
