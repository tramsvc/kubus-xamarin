﻿using KUBus.Views;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using Xamarin.Forms;
using System.Threading.Tasks;

namespace KUBus.ViewModels
{
    public class QRScanViewModel : Base.ViewModelBase, INotifyPropertyChanged
    {
        private ZXing.Result result;
        public ZXing.Result Result
        {
            get { return result; }
            set
            {
                if (!Equals(result, value))
                {
                    result = value;
                    OnPropertyChanged(nameof(Result));
                }
            }
        }

        private bool isAnalyzing = true;
        public bool IsAnalyzing
        {
            get { return isAnalyzing; }
            set
            {
                if (!Equals(isAnalyzing, value))
                {
                    isAnalyzing = value;
                    OnPropertyChanged(nameof(IsAnalyzing));
                }
            }
        }

        private bool isScanning = true;
        public bool IsScanning
        {
            get { return isScanning; }
            set
            {
                if (!Equals(isScanning, value))
                {
                    isScanning = value;
                    OnPropertyChanged(nameof(IsScanning));
                }
            }
        }

        public QRScanViewModel()
        {
            isScanning = true;
            isAnalyzing = true;
        }

        public Command QRScanResultCommand
        {
            get
            {
                return new Command(() =>
                {
                    IsAnalyzing = false;
                    IsScanning = false;

                    Device.BeginInvokeOnMainThread(async () =>
                    {
                        await Application.Current.MainPage.DisplayAlert("Alert Test", Result.Text, "OK", "Cancel");
                        //await Application.Current.MainPage.Navigation.PushAsync(new MapPage());
                    });
                });
            }
        }
    }
}
