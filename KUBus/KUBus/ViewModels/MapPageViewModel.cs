﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;
using System.ComponentModel;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

using System.Diagnostics;


namespace KUBus.ViewModels
{
    public class MapPageViewModel : Base.ViewModelBase
    {
        List<string> AddressPoints { get; set; } = new List<string>()
        {
            "14.023626, 99.977665",
            "14.022622, 99.973760",
            "14.019603, 99.969329"
        };

        public List<string> MockList = new List<string>()
        {
            "14.035735, 99.985366",
            "14.013347, 99.970294"
        };

        public List<string> AddressPointList
        {
            get { return AddressPoints; }
            set
            {
                AddressPoints = value;
                OnPropertyChanged();
            }
        }

        public MapPageViewModel()
        {
            IsBusy = true;

            foreach (var x in AddressPointList)
            {
                Debug.WriteLine(x);
            }
            
            Device.StartTimer(TimeSpan.FromSeconds(7), () =>
            {
                AddressPointList = MockList;
                IsBusy = false;
                return false;
            });

            
        }
        public MapPageViewModel(List<string> MockCoordList)
        {
            IsBusy = true;
            AddressPointList = MockCoordList;
            IsBusy = false;
        }
    }
}
