﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Runtime.CompilerServices;

using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Acr.UserDialogs;
using Xamarin.Forms;

namespace KUBus.ViewModels.Base
{
    public class ViewModelBase : INotifyPropertyChanged
    {
        private bool _isBusy;
        public bool IsConnected { get; private set; }
        public IUserDialogs PageDialogue { get; }
        private IConnectivity connectivity = CrossConnectivity.Current;
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// IsBusy Property for use with ActivityIndicator
        /// </summary>
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                _isBusy = value;
                OnPropertyChanged();
            }
        }

        public ViewModelBase()
        {
            PageDialogue = UserDialogs.Instance;
            IsConnected = connectivity.IsConnected;
            connectivity.ConnectivityChanged += OnConnectivityChanged;
        }

        private void OnConnectivityChanged(object sender, ConnectivityChangedEventArgs e)
        {
            IsConnected = e.IsConnected;
            if (!IsConnected)
            {
                var stringResponse = "A problem has occurred with your network connection.";
                PageDialogue.Toast(stringResponse);
            }
        }

        public void OnPropertyChanged([CallerMemberName] string name = "")
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
        }  
    }
}
