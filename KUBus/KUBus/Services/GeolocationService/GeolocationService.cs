﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;

using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace KUBus.Services.GeolocationService
{
    public class GeolocationService
    {
        public static async Task<Position> GetCurrentPostion()
        {
            Position position = null;
            try
            {
                var locator = CrossGeolocator.Current;
                locator.DesiredAccuracy = 100;
                position = await locator.GetLastKnownLocationAsync();

                if(position != null)
                {
                    //Return Cached Position
                    return position;
                }
                if (!locator.IsGeolocationAvailable || !locator.IsGeolocationEnabled)
                {
                    //not available or enabled
                    return null;
                }
                position = await locator.GetPositionAsync(TimeSpan.FromSeconds(20),null,true);
            }
            catch { }

            if (position == null)
            {
                return null;
            }

            var output_debug = string.Format("Time: {0} \nLat: {1} \nLong: {2} \nAltitude: {3} \nAltitude Accuracy: {4} \nAccuracy: {5} \nHeading: {6} \nSpeed: {7}",
                    position.Timestamp.ToLocalTime(), position.Latitude, position.Longitude,
                    position.Altitude, position.AltitudeAccuracy, position.Accuracy, position.Heading, position.Speed);
            Debug.WriteLine(output_debug);
            return position;
        }
    }
}
