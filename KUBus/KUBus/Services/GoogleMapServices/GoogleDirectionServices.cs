﻿using KUBus.Models;
using Newtonsoft.Json.Linq;

using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace KUBus.Services.GoogleMapServices
{
    public class GoogleDirectionServices
    {
        public static async Task<JObject> GetDirectionFromGoogleAPI(string origin, string destination)
        {
            string url = "https://maps.googleapis.com/maps/api/directions/json";
            string aditionnal_URL = "?origin=" + origin
            + "&destination=" + destination
            + "&key=" + App.Google_Map_API_Key;

            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(url);

                var content = new StringContent("{}", Encoding.UTF8, "application/json");
                HttpResponseMessage response = null;
                try
                {
                    response = await client.PostAsync(aditionnal_URL, content);
                }
                catch (Exception)
                {
                    return (null);
                }
                string result = await response.Content.ReadAsStringAsync();
                if (result != null)
                {
                    try
                    {
                        return JObject.Parse(result);
                    }
                    catch (Exception)
                    {
                        return (null);
                    }
                }
                else
                {
                    return (null);
                }
            }
            catch (Exception)
            {
                return (null);
            }
        }
    }
}
