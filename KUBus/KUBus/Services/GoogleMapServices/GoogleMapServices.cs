﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms.Maps;

using Newtonsoft.Json.Linq;
using System.Net.Http;

namespace KUBus.Services.GoogleMapServices
{
    public class GoogleMapServices
    {
        private static async Task<JObject> GoogleAPIHttpRequestAsync(string url, string additionnal_URL)
        {
            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri(url);

                var content = new StringContent("{}", Encoding.UTF8, "application/json");
                HttpResponseMessage response = null;
                try
                {
                    response = await client.PostAsync(additionnal_URL, content);
                }
                catch (Exception)
                {
                    return (null);
                }
                string result = await response.Content.ReadAsStringAsync();
                if (result != null)
                {
                    try
                    {
                        return JObject.Parse(result);
                    }
                    catch (Exception)
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static async Task<string> GetAddressName(Position position)
        {
            string url = "https://maps.googleapis.com/maps/api/geocode/json";
            string additionnal_URL = "?latlng=" + position.Latitude + "," + position.Longitude
            + "&key=" + App.Google_Map_API_Key;

            JObject obj = await GoogleAPIHttpRequestAsync(url, additionnal_URL);

            string address_name;
            try
            {
                address_name = (obj["results"][0]["formatted_address"]).ToString();
            }
            catch (Exception)
            {
                return ("");
            }
            return (address_name);
        }
    }
}
