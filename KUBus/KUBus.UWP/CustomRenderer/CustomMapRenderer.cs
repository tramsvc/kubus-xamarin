﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Windows.Devices.Geolocation;
using Windows.UI;
using Windows.UI.Xaml.Controls.Maps;

using KUBus.Controls;
using KUBus.UWP.CustomRenderer;

using Xamarin.Forms.Maps;
using Xamarin.Forms.Maps.UWP;
using Xamarin.Forms.Platform.UWP;
using System.ComponentModel;

[assembly: ExportRenderer(typeof(CustomMap), typeof(CustomMapRenderer))]
namespace KUBus.UWP.CustomRenderer
{
    public class CustomMapRenderer : MapRenderer
    {
        /// <summary>
        /// Instance of native map.
        /// </summary>
        MapControl nativeMap;

        /// <summary>
        /// Instance of Custom Map in .NET Shared Code lib.
        /// </summary>
        CustomMap customMap;

        

        protected override void OnElementChanged(ElementChangedEventArgs<Map> e)
        {
            base.OnElementChanged(e);

            if(e.OldElement != null)
            {
                //Unsubscribe
            }

            if(e.NewElement != null)
            {
                customMap = e.NewElement as CustomMap;
                nativeMap = Control as MapControl;
                nativeMap.Loaded += ((sender, re) =>
                {
                    customMap.MapLoaded();
                });
                UpdatePolyline();
            }
            
        }

        protected override void OnElementPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            base.OnElementPropertyChanged(sender, e);

            if(Element == null || Control == null)
            {
                return;
            }

            if (e.PropertyName == CustomMap.PolylineCoordinatesProperty.PropertyName
                || e.PropertyName == CustomMap.PolylineColorProperty.PropertyName
                || e.PropertyName == CustomMap.PolylineThicknessProperty.PropertyName)
            {
                UpdatePolyline();
            }
                
        }

        private void UpdatePolyline()
        {
            if(customMap != null && nativeMap != null)
            {
                if(customMap.PolylineCoordinates != null && customMap.PolylineCoordinates.Count > 0)
                {
                    List<BasicGeoposition> coordinates = new List<BasicGeoposition>();

                    foreach (var position in customMap.PolylineCoordinates)
                    {
                        coordinates.Add(new BasicGeoposition() { Latitude = position.Latitude, Longitude = position.Longitude });
                    }

                    Geopath path = new Geopath(coordinates);
                    MapPolyline polyline = new MapPolyline();
                    polyline.StrokeColor = new Color()
                    {
                        A = (byte)(customMap.PolylineColor.A * 255),
                        R = (byte)(customMap.PolylineColor.R * 255),
                        G = (byte)(customMap.PolylineColor.G * 255),
                        B = (byte)(customMap.PolylineColor.B * 255)
                    };
                    polyline.StrokeThickness = customMap.PolylineThickness;
                    polyline.Path = path;
                    nativeMap.MapElements.Clear();
                    nativeMap.MapElements.Add(polyline);
                    nativeMap.UpdateLayout();
                }
            }
        }
    }
}
