﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace KUBus.UWP
{
    public sealed partial class MainPage
    {
        public MainPage()
        {
            InitializeComponent();
            LoadApplication(new KUBus.App());
            ZXing.Net.Mobile.Forms.WindowsUniversal.ZXingScannerViewRenderer.Init();
            Xamarin.FormsMaps.Init("MfnP95B1Ex53CrFHYjtc~8mJm0WtPzU0F8NT3yC3aGQ~AhzJlHw9S_EOu9DNYgQciGh_Q_b-obXYBtnJBEtVonkNOFYi_bvq_x11p_pLkJsq");
        }
    }
}
